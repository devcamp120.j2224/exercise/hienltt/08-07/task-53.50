package com.devcamp.j04_javabasic.interfaceclass;

public interface ISumable {
	String  getSum();
}

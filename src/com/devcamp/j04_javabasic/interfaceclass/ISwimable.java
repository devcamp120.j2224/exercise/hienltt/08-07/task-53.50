package com.devcamp.j04_javabasic.interfaceclass;

public interface ISwimable {
	void swim();
}

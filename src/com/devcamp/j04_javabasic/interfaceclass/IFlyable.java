package com.devcamp.j04_javabasic.interfaceclass;

public interface IFlyable {
	void fly();
}

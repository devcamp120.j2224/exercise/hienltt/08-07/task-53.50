package com.devcamp.j04_javabasic.interfaceclass;

public interface Other {
 void other();
 int other(int param);
 String other(String param);
}

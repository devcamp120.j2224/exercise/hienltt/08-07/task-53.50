package com.devcamp.j04_javabasic;

public enum AnimalClass {
	fish, amphibians, reptiles, mammals, birds
}

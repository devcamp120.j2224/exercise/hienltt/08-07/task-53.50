package com.devcamp.j04_javabasic;

public abstract class CAnimal {
	public AnimalClass animclass;
	
	abstract public void animalSound();
	
	public void eat() {
		System.out.println("Animal eating...");
	};
	
}

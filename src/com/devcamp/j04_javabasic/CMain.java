package com.devcamp.j04_javabasic;

import java.util.ArrayList;

public class CMain {

	public static void main(String[] args) {
		CPet dog = new CDog(1, "Lulu");

		CPet cat = new CCat(2, "Milo");

		ArrayList<CPet> petsList = new ArrayList<>();
		petsList.add(dog);
		petsList.add(cat);

		CPerson person1 = new CPerson();
		CPerson person2 = new CPerson(1, 20, "Le Van", "Thang", petsList);

		System.out.println(person1.getFirstName());
		System.out.println(person2.getFirstName());
		System.out.println(person2.getAge());

	} 
}


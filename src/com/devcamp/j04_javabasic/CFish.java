package com.devcamp.j04_javabasic;

import com.devcamp.j04_javabasic.interfaceclass.ISwimable;

public class CFish extends CPet implements ISwimable {
	@Override
	public void swim() {
		System.out.println("fish swiming.");
	}
	public void animalSound() {
		// TODO Auto-generated method stub
		System.out.println("Fish sound...");
	}
}

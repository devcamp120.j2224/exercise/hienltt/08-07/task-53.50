package com.devcamp.j04_javabasic.interfaceclass;

public interface IRunable extends IBarkable {
	void run() ;
	void running() ;
}

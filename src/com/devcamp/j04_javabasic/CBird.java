package com.devcamp.j04_javabasic;

import com.devcamp.j04_javabasic.interfaceclass.IFlyable;

public class CBird extends CPet implements IFlyable {
	@Override
	public void fly() {
		System.out.println("Bird flying");
	}
	public void animalSound() {
		// TODO Auto-generated method stub
		System.out.println("Bird sound...");
	}
}
